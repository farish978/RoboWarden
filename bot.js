const Discord = require("discord.js");
const Chance = require("chance");
const fetch = require("node-fetch");
const chalk = require("chalk");
const Dictionary = require('oxford-dictionary');
const Fuse = require('fuse.js');


require("dotenv").config();
const mongo = require("./database/dbConnect.js");
const bot = new Discord.Client({ partials: ["MESSAGE", "REACTION", "CHANNEL"], });
const chanceObj = new Chance();
var dictConfiguration = {
  app_id: process.env.dict_appID,
  app_key: process.env.dict_appKey,
  source_lang: "en-us"
};
var dict = new Dictionary(dictConfiguration);

const databaseImport = require("./database/schemas/dbImport");
const messageCounter = require("./database/events/messageCounter");

const PREFIX = "!";
const GROUPPREFIX = "<@&762087998187503666>";
const active = 'active'
const inactive = 'inactive'

messageCounter(bot);

//log
bot.on("ready", async () => {
  await mongo();

  console.log(chalk.green("RoboWarden is online."));
  console.log(chalk.green("Connected to database."));

  // console.log(process.env);
  //  bot.user.setActivity("over Yaquta's Prison", { type: 'WATCHING'});
  const botonnotifchannel = bot.channels.cache.get("728422907218493524");
  botonnotifchannel.send("restart_successful");
  botonnotifchannel.send("Database connection establised.");

  const botonnotifchannelCF = bot.channels.cache.get("742135567210381362");
  botonnotifchannelCF.send("restart_successful");
});

//           <.........  Functional Features Starts Here .........>

// Greetings with new inmates role
bot.on("guildMemberAdd", async (member) => {

  const messageCounterSchema = require('./database/schemas/messageCountSchema');
  var role = member.guild.roles.cache.get("727664329222258730");
  var parolerole = member.guild.roles.cache.get("713007999009947648");
  const channel = member.guild.channels.cache.get("727675902456234036");
  const newinmatechannel = member.guild.channels.cache.get("712934277939200032");
  const officerRole = member.guild.roles.cache.get("712754991521333319");
  const gateKeeperRole = member.guild.roles.cache.get("715614871814799383");




  const newMember = await messageCounterSchema.find({
    _id: member.id
  });

  if (newMember.length > 0) {
    const [{ paroleStatus, roles, paroleChannelId }] = newMember;
    const newInmateParoleChannel = bot.channels.cache.get(paroleChannelId);

    console.log(newMember)
    console.log(roles)

    if (paroleStatus === active) {
      member.roles.add(parolerole);
      // member.roles.add(roles);

      newInmateParoleChannel.overwritePermissions([
        {
          allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'ATTACH_FILES', 'EMBED_LINKS', 'ADD_REACTIONS', 'USE_EXTERNAL_EMOJIS', 'READ_MESSAGE_HISTORY'],
          id: member.id
        },
        {
          deny: 'VIEW_CHANNEL',
          id: member.guild.id
        },
        {
          allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'READ_MESSAGE_HISTORY', 'USE_EXTERNAL_EMOJIS', 'ATTACH_FILES', 'EMBED_LINKS', 'ADD_REACTIONS'],
          id: officerRole
        },
        {
          allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'READ_MESSAGE_HISTORY', 'USE_EXTERNAL_EMOJIS', 'ATTACH_FILES', 'EMBED_LINKS', 'ADD_REACTIONS'],
          id: gateKeeperRole
        }
      ]);
      newInmateParoleChannel.send(`${member}, you cannot be permitted back to the server without someone permitting you from parole.`);
      newinmatechannel.send(`${member} is back on the server and is on parole.`)
    }
  } else {
    member.roles.add(role);
    channel.send(`Welcome ${member}, you are now at the entry gate of one of the most secretive channels on discord. I know you can't see a lot here but hang on till <@&715614871814799383> is here to interrogate you. You will be notified so don't worry. Once you're confirmed to be an Inmate, the officer will grant you full access to this server. In the mean time, head to <#733319986264801290> and checkout some rules that you need to strictly follow.`);
  }

  member.roles.add(role);
  // var role2 = member.guild.roles.cache.get("720213022076829728");
  //   member.roles.add(role2)
});

// Member leave notification
bot.on("guildMemberRemove", (member) => {
  if(member.guild.id !== '711634547770654791') return;
  const newinmatechannel = member.guild.channels.cache.get("712934277939200032");
  newinmatechannel.send(`${member} just left the server.`);
});


const regionCommandCooldown = new Set();

const usersMap = new Map();
const LIMIT = 5;
const TIME = 30000;
const DIFF = 3000;

const groupCallSet = new Set();
const groupCallerSet = new Set();

// role assingment command
// const botCommands = require('./events/commands/commands.js');
// botCommands(bot);
// role assingment command
// role assingment command
bot.on("message", async (message) => {
  const messageContentLowercase = message.content.toLowerCase();
  const sentencechannel = message.guild.channels.cache.get("727551487915327488");
  const newinmatechannel = message.guild.channels.cache.get("712934277939200032");
  const welcomechannel = message.guild.channels.cache.get("715340560826630174");
  const entrygatechannel = message.guild.channels.cache.get("727675902456234036");
  // const parolechannel = message.guild.channels.cache.get('712640459473813574');
  const chatChannel = message.guild.channels.cache.get("712390649151881288");
  const chatBotChannel = message.guild.channels.cache.get("737489384357494906");
  const birthdayCommandChannel = message.guild.channels.cache.get("737398480233955438");
  const blockChat = message.guild.channels.cache.get("723646089399369869");
  const bdayNotifChannel = message.guild.channels.cache.get("737517982732779591");
  const betatestChannel = message.guild.channels.cache.get("712679003005386832");
  const musicCommandsChannel = message.guild.channels.cache.get("712684598123036806");
  const modmusicCommandsChannel = message.guild.channels.cache.get("712680589592887297");
  const officerRole = message.guild.roles.cache.get("712754991521333319");
  const gateKeeperRole = message.guild.roles.cache.get("715614871814799383");
  const parolecategory = "737682411818582098";
  const archiveCategory = "728559757513850941";
  const parolearchivecategory = "737630812517826610";
  const betatestParentId = '712678821035507774'
  const rythm1ID = message.guild.members.cache.get("235088799074484224");
  const rythm2ID = message.guild.members.cache.get("252128902418268161");
  const groupSchema = require("./database/schemas/groupSchema");
  const riddleChannel = message.guild.channels.cache.get("751362510938177567");
  const wChannel = bot.channels.cache.get("706646103541547070");
  const blockchat = message.guild.channels.cache.get("723646089399369869");
  const headofficechannel = message.guild.channels.cache.get("711634548303069297");
  const officechannel = message.guild.channels.cache.get("715698435130654790");
  const colorSelectChannel = message.guild.channels.cache.get("757561543708901436");
  const chatChannel_hook = bot.fetchWebhook('738412583199047805', '29URNGDfOI1IxzwO9EuTRaPaMvGAI0py-aJOnEQv59kS6WqekMz_j1GZFDG3Px3w7lWZ');
  const blockChat_hook = bot.fetchWebhook('738464186086064218', 'zpwCrsKU3Xzogi7gAD_MbY5OUETzTdQuyFzIQI8dtbys_bGzO3A7hLfFLgUbcVMZ7_9p');
  const messageCounterSchema = require('./database/schemas/messageCountSchema');
  const botCommandsChannel = message.guild.channels.cache.get('766493706190454814');
  const movienightsChannel = message.guild.channels.cache.get('715255733666709554');
  const movieNightsSchema = require('./database/schemas/movieNightsSchema');
 
  // if (message.author.bot) return;
  // if(message.guild.id !== '711634547770654791') return;
  if (message.channel.type === "dm") return;
  // console.log(`${message.member.displayName} in ${message.channel.name}: ${message.content}`);

  // bot commands
  if (message.content.startsWith(PREFIX)) {
    let args = message.content.substring(PREFIX.length).split(" ");
    switch (args[0]) {

      case 'archive':

        if (message.member.hasPermission('ADMINISTRATOR')) {
          message.channel.setParent(archiveCategory).then(() => message.channel.setPosition(0));
          message.react("👍");
          newinmatechannel.send(`${message.author} has archived ${message.channel} channel.`);
        }

        break;

      case 'movietime':

        const movietimeCommand = message.content.replace(`${PREFIX}movietime `, '')

        if (movietimeCommand.startsWith('https://discord.com/channels/')) {

          const messageId = movietimeCommand.replace(`https://discord.com/channels/${message.guild.id}/${movienightsChannel.id}/`, '');

          if (message.member.roles.cache.has('735003388101525511') || message.member.hasPermission('MANAGE_NICKNAMES')) {
            if (message.channel === blockChat || message.channel === chatChannel || message.channel.parentID === betatestParentId) {

              const movieAnnouncement = await movieNightsSchema.find({
                _id: messageId
              });

              if (movieAnnouncement.length > 0) {

                const [{ reactedUserIds }] = movieAnnouncement;

                var movieCalls = "";
                for (i in reactedUserIds) {
                  movieCalls += "<@!" + reactedUserIds[i] + "> ";
                }
                message.channel.send(`**Movie Time!** ${movieCalls}`);

              }
            } else
              return;
          } else {
            return;
          }
        } else if (movietimeCommand.startsWith('https://discordapp.com/channels/')) {

          const messageId = movietimeCommand.replace(`https://discordapp.com/channels/${message.guild.id}/${movienightsChannel.id}/`, '');

          if (message.member.roles.cache.has('735003388101525511') || message.member.hasPermission('MANAGE_NICKNAMES')) {
            if (message.channel === blockChat || message.channel === chatChannel || message.channel.parentID === betatestParentId) {

              const movieAnnouncement = await movieNightsSchema.find({
                _id: messageId
              });

              if (movieAnnouncement.length > 0) {

                const [{ reactedUserIds }] = movieAnnouncement;

                var movieCalls = "";
                for (i in reactedUserIds) {
                  movieCalls += "<@!" + reactedUserIds[i] + "> ";
                }
                message.channel.send(`**Movie Time!** ${movieCalls}`);

              }
            } else
              return;
          } else {
            return;
          }
        } else {
          message.reply(`I cannot understand that. Here's how you do this command.` + "```" + `${PREFIX}movietime https://discord.com/channels/711634547770654791/715255733666709554/766518372165877770` + "```")
        }

        break;

      case 'talk':
        const talk = message.content.replace('!talk ', '');

        if (message.member.hasPermission('MANAGE_NICKNAMES')) {

          message.channel.send(talk);
          message.delete();

        } else {
          return;
        }

        break;

      case 'stop':

        if (message.member.hasPermission('MANAGE_NICKNAMES')) {
          message.channel.stopTyping();
        }

        break;

      case 'udict':

        if (message.channel === botCommandsChannel || message.channel.parent.id === '712678821035507774' || message.channel.parent.id === '754770774837035038' || message.channel.parent.id === '485739480242978817') {


          const udictQuesry = message.content.replace('!udict ', '');

          message.channel.startTyping();
          message.channel.stopTyping();

          const udict = await fetch(`http://api.urbandictionary.com/v0/define?term=${udictQuesry}`).then((definition) => definition.json());

          const data = udict.list

          if (data.length > 0) {

            const highestThumbsUp = data.reduce((lowest, definition) => definition.thumbs_up > lowest.thumbs_up ? definition : lowest, data[0]);

            const { definition, example } = highestThumbsUp

            message.channel.send("```" + definition + "``````\n" + example + "```");

          } else {
            return message.channel.send(`Nope! Can't find that one.`);
          }

        } else {
          return;
        };

        break;

      case 'dict':


        if (message.channel === botCommandsChannel || message.channel.parent.id === '712678821035507774' || message.channel.parent.id === '754770774837035038' || message.channel.parent.id === '485739480242978817') {

          const dictQuesry = messageContentLowercase.replace('!dict ', '');

          message.channel.startTyping();

          const lookup = dict.definitions(dictQuesry);

          lookup.then(function (res) {

            const [lexicalEntries] = res.results
            const [entries] = lexicalEntries.lexicalEntries
            const [{ senses }] = entries.entries
            const [definitions] = senses

            const randomAccordance = chanceObj.pickone([
              `According to the Oxford Dictionaries`,
              'I think',
              'I checked and found out that',
              `Here's what I found`,
              'I believe'
            ]);

            message.channel.send(`${randomAccordance}, __${dictQuesry}__ is, ${definitions.definitions}.`)
          },
            function (err) {

              const randomDictError = chanceObj.pickone([
                'Are you kidding me?',
                "That's not even a word.",
                "I'm sorry but I couldn't find that one for you.",
                `I do not have human level skillset to take ${dictQuesry} as a word and make a definition for it.`
              ]);
              if (err) return message.channel.send(randomDictError).then(message.channel.stopTyping());
            });
          message.channel.stopTyping();

        } else {
          message.react('👎');
          message.reply('this command is limited to work only in some specific channels.').then(m => m.delete({ timeout: 10000 }))
          message.delete({ timeout: 10000 });
        };

        break;

      case "listbadges":
        const badgesListDB = require("./database/schemas/messageCountSchema");

        var person = message.mentions.members.first();
        if (!person)
          return message.channel
            .send("Please specify a valid user.")
            .then((m) => m.delete({ timeout: 5000 }))
            .then(
              message.delete({ timeout: 5000 }).catch((err) => console.log(err))
            );

        if (person.id === bot.user.id)
          return message
            .react("👎")
            .then(
              message.delete({ timeout: 5000 }).catch((err) => console.log(err))
            );

        if (person.id === message.author.id)
          return message
            .react("👎")
            .then(
              message.delete({ timeout: 5000 }).catch((err) => console.log(err))
            );

        if (person.hasPermission("MANAGE_NICKNAMES"))
          return message
            .react("👎")
            .then(
              message.delete({ timeout: 5000 }).catch((err) => console.log(err))
            );

        // var inmaterole = person.guild.roles.cache.get("712289127948877844");
        // var parolerole = person.guild.roles.cache.get("713007999009947648");
        // let newinmaterole = person.guild.roles.cache.get("727664329222258730");

        if (message.member.hasPermission("MANAGE_NICKNAMES")) {
          if (message.channel === entrygatechannel) {
            // message.channel.startTyping();

            const badgesList = await badgesListDB.find({
              _id: person.id,
            });

            const [{ roles }] = badgesList;

            //embed
            var newDisplay = "";
            for (i in roles) {
              newDisplay += "<@&" + roles[i] + ">\n";
            }

            let embed = new Discord.MessageEmbed()
              .addField(`Badges\n`, newDisplay)
              .setTitle(`__Badge History for ${person.displayName}__`)
              .setDescription(
                `Here is the list of badges ${person.displayName} owns.\nPlease crosscheck with this list of badges ${person.displayName} owned, previously.`
              )
              .setThumbnail(person.user.avatarURL());
            message.channel.send(embed);

            // message.channel.stopTyping();
          }

          if (message.channel.parentID === parolecategory) {
            // message.channel.startTyping();

            const badgesList = await badgesListDB.find({
              _id: person.id,
            });

            const [{ roles }] = badgesList;

            //embed
            var newDisplay = "";
            for (i in roles) {
              newDisplay += "<@&" + roles[i] + ">\n";
            }

            let embed = new Discord.MessageEmbed()
              .addField(`Badges\n`, newDisplay)
              .setTitle(`__Badge History for ${person.displayName}__`)
              .setDescription(
                `Here is the list of badges ${person.displayName} owns..\nPlease crosscheck with this list of badges ${person.displayName} owned, previously.`
              )
              .setThumbnail(person.user.avatarURL());
            message.channel.send(embed);

            // message.channel.stopTyping();
          }
        } else {
          return;
        }

        break;

      case "badges":
        const badgesDB = require("./database/schemas/messageCountSchema");

        var person = message.mentions.members.first();
        if (!person)
          return message.channel
            .send("Please specify a valid user.")
            .then((m) => m.delete({ timeout: 5000 }))
            .then(
              message.delete({ timeout: 5000 }).catch((err) => console.log(err))
            );

        if (person.id === bot.user.id)
          return message
            .react("👎")
            .then(
              message.delete({ timeout: 5000 }).catch((err) => console.log(err))
            );

        if (person.id === message.author.id)
          return message
            .react("👎")
            .then(
              message.delete({ timeout: 5000 }).catch((err) => console.log(err))
            );

        if (person.hasPermission("MANAGE_NICKNAMES"))
          return message
            .react("👎")
            .then(
              message.delete({ timeout: 5000 }).catch((err) => console.log(err))
            );

        var inmaterole = person.guild.roles.cache.get("712289127948877844");
        var parolerole = person.guild.roles.cache.get("713007999009947648");
        let newinmaterole = person.guild.roles.cache.get("727664329222258730");

        if (message.member.hasPermission("MANAGE_NICKNAMES")) {
          if (message.channel === entrygatechannel) {
            // message.channel.startTyping();

            const badgesList = await badgesDB.find({
              _id: person.id,
            });

            const [{ roles }] = badgesList;

            //embed
            var newDisplay = "";
            for (i in roles) {
              newDisplay += "<@&" + roles[i] + ">\n";
            }

            let embed = new Discord.MessageEmbed()
              .addField(`Badges\n`, newDisplay)
              .setTitle(`__Badge History for ${person.displayName}__`)
              .setDescription(
                `I've added the badges for ${person.displayName}.\nPlease crosscheck with this list of badges ${person.displayName} owned, previously.`
              )
              .setThumbnail(person.user.avatarURL());
            message.channel.send(embed);

            // console.log(roles);

            person.roles.add(roles);
            person.roles.remove(parolerole);
            person.roles.remove(inmaterole);
            message.react("👍");

            // message.channel.stopTyping();
          }

          if (message.channel.parentID === parolecategory) {
            // message.channel.startTyping();

            const badgesList = await badgesDB.find({
              _id: person.id,
            });

            const [{ roles }] = badgesList;

            //embed
            var newDisplay = "";
            for (i in roles) {
              newDisplay += "<@&" + roles[i] + ">\n";
            }

            let embed = new Discord.MessageEmbed()
              .addField(`Badges\n`, newDisplay)
              .setTitle(`__Badge History for ${person.displayName}__`)
              .setDescription(
                `I've added the badges for ${person.displayName}.\nPlease crosscheck with this list of badges ${person.displayName} owned, previously.`
              )
              .setThumbnail(person.user.avatarURL());
            message.channel.send(embed);

            person.roles.add(roles);
            person.roles.add(parolerole);
            person.roles.remove(newinmaterole);
            message.react("👍");

            // message.channel.stopTyping();
          }
        } else {
          return;
        }

        break;

      case "region":
        if (message.member.hasPermission('MANAGE_NICKNAMES')) {
          if (regionCommandCooldown.has(PREFIX)) {
            if (message.channel == blockChat) {
              if (message.guild.region === "europe") {
                message.guild.setRegion("india").then((updated) => {
                  newinmatechannel.send(
                    `${message.author} updated server region to ${updated.region}`
                  );
                  message.channel
                    .send(`I've updated server region to ${updated.region}`)
                    .then((m) => m.delete({ timeout: 5000 }));
                  message.react("👍");

                  regionCommandCooldown.delete(PREFIX);
                  regionCommandCooldown.add(PREFIX);
                  setTimeout(() => {
                    regionCommandCooldown.delete(PREFIX)
                  }, 1800000);
                });
              } else {
                message.guild.setRegion("europe").then((updated) => {
                  newinmatechannel.send(
                    `${message.author} updated server region to ${updated.region}`
                  );
                  message.channel
                    .send(`I've updated server region to ${updated.region}`)
                    .then((m) => m.delete({ timeout: 5000 }));
                  message.react("👍");

                  regionCommandCooldown.delete(PREFIX);
                  regionCommandCooldown.add(PREFIX);
                  setTimeout(() => {
                    regionCommandCooldown.delete(PREFIX)
                  }, 1800000);
                });
              }
            }

            if (message.channel == musicCommandsChannel) {
              if (message.guild.region === "europe") {
                message.guild.setRegion("india").then((updated) => {
                  newinmatechannel.send(
                    `${message.author} updated server region to ${updated.region}`
                  );
                  message.channel
                    .send(`I've updated server region to ${updated.region}`)
                    .then((m) => m.delete({ timeout: 5000 }));
                  message.react("👍");

                  regionCommandCooldown.delete(PREFIX);
                  regionCommandCooldown.add(PREFIX);
                  setTimeout(() => {
                    regionCommandCooldown.delete(PREFIX)
                  }, 1800000);
                });
              } else {
                message.guild.setRegion("europe").then((updated) => {
                  newinmatechannel.send(
                    `${message.author} updated server region to ${updated.region}`
                  );
                  message.channel
                    .send(`I've updated server region to ${updated.region}`)
                    .then((m) => m.delete({ timeout: 5000 }));
                  message.react("👍");

                  regionCommandCooldown.delete(PREFIX);
                  regionCommandCooldown.add(PREFIX);
                  setTimeout(() => {
                    regionCommandCooldown.delete(PREFIX)
                  }, 1800000);
                });
              }
            }
          } else {

            if (message.channel == blockChat) {
              if (message.guild.region === "europe") {
                message.guild.setRegion("india").then((updated) => {
                  newinmatechannel.send(
                    `${message.author} updated server region to ${updated.region}`
                  );
                  message.channel
                    .send(`I've updated server region to ${updated.region}`)
                    .then((m) => m.delete({ timeout: 5000 }));
                  message.react("👍");

                  regionCommandCooldown.add(PREFIX);
                  setTimeout(() => {
                    regionCommandCooldown.delete(PREFIX)
                  }, 1800000);
                });
              } else {
                message.guild.setRegion("europe").then((updated) => {
                  newinmatechannel.send(
                    `${message.author} updated server region to ${updated.region}`
                  );
                  message.channel
                    .send(`I've updated server region to ${updated.region}`)
                    .then((m) => m.delete({ timeout: 5000 }));
                  message.react("👍");

                  regionCommandCooldown.add(PREFIX);
                  setTimeout(() => {
                    regionCommandCooldown.delete(PREFIX)
                  }, 1800000);
                });
              }
            }

            if (message.channel == musicCommandsChannel) {
              if (message.guild.region === "europe") {
                message.guild.setRegion("india").then((updated) => {
                  newinmatechannel.send(
                    `${message.author} updated server region to ${updated.region}`
                  );
                  message.channel
                    .send(`I've updated server region to ${updated.region}`)
                    .then((m) => m.delete({ timeout: 5000 }));
                  message.react("👍");

                  regionCommandCooldown.add(PREFIX);
                  setTimeout(() => {
                    regionCommandCooldown.delete(PREFIX)
                  }, 1800000);
                });
              } else {
                message.guild.setRegion("europe").then((updated) => {
                  newinmatechannel.send(
                    `${message.author} updated server region to ${updated.region}`
                  );
                  message.channel
                    .send(`I've updated server region to ${updated.region}`)
                    .then((m) => m.delete({ timeout: 5000 }));
                  message.react("👍");

                  regionCommandCooldown.add(PREFIX);
                  setTimeout(() => {
                    regionCommandCooldown.delete(PREFIX)
                  }, 1800000);
                });
              }
            }

          }
        } else {
          if (regionCommandCooldown.has(PREFIX)) {
            message.reply(`this command was run within the past 30 minutes. Please wait for a while before you can try this command.`).then(m => m.delete({ timeout: 10000 }));
            message.react('👎');
            return;
          } else {
            if (message.channel == blockChat) {
              if (message.guild.region === "europe") {
                message.guild.setRegion("india").then((updated) => {
                  newinmatechannel.send(
                    `${message.author} updated server region to ${updated.region}`
                  );
                  message.channel
                    .send(`I've updated server region to ${updated.region}`)
                    .then((m) => m.delete({ timeout: 5000 }));
                  message.react("👍");

                  regionCommandCooldown.add(PREFIX);
                  setTimeout(() => {
                    regionCommandCooldown.delete(PREFIX)
                  }, 1800000);
                });
              } else {
                message.guild.setRegion("europe").then((updated) => {
                  newinmatechannel.send(
                    `${message.author} updated server region to ${updated.region}`
                  );
                  message.channel
                    .send(`I've updated server region to ${updated.region}`)
                    .then((m) => m.delete({ timeout: 5000 }));
                  message.react("👍");

                  regionCommandCooldown.add(PREFIX);
                  setTimeout(() => {
                    regionCommandCooldown.delete(PREFIX)
                  }, 1800000);
                });
              }
            }

            if (message.channel == musicCommandsChannel) {
              if (message.guild.region === "europe") {
                message.guild.setRegion("india").then((updated) => {
                  newinmatechannel.send(
                    `${message.author} updated server region to ${updated.region}`
                  );
                  message.channel
                    .send(`I've updated server region to ${updated.region}`)
                    .then((m) => m.delete({ timeout: 5000 }));
                  message.react("👍");

                  regionCommandCooldown.add(PREFIX);
                  setTimeout(() => {
                    regionCommandCooldown.delete(PREFIX)
                  }, 1800000);
                });
              } else {
                message.guild.setRegion("europe").then((updated) => {
                  newinmatechannel.send(
                    `${message.author} updated server region to ${updated.region}`
                  );
                  message.channel
                    .send(`I've updated server region to ${updated.region}`)
                    .then((m) => m.delete({ timeout: 5000 }));
                  message.react("👍");

                  regionCommandCooldown.add(PREFIX);
                  setTimeout(() => {
                    regionCommandCooldown.delete(PREFIX)
                  }, 1800000);
                });
              }
            }
          }
        }

        break;

      case "prisonplaylist":
        const rapPlaylistt =
          "https://open.spotify.com/playlist/6maRWICwEMaoRRtKagoLt4?si=ejJ6TtniRIWABhQ3Awwh-g";
        const allRockPlaylist =
          "https://open.spotify.com/playlist/4K1q7HfRED5vTmScz2B3oR?si=EAWCEZcVTn2OQ9WgX70www";
        const countryPlaylist =
          "https://open.spotify.com/playlist/2zbHBtnstfkkY0f7Ow2LrQ?si=XQL3oJ_TSby42oBIUzYcLA";
        const fridayeveningPlaylist =
          "https://open.spotify.com/playlist/2GqAiv9f6ag9zUF4wQYSk2?si=Iza1Jq-aS2yCybv-K3_Ttg";

        if (message.member.hasPermission("MANAGE_NICKNAMES")) {
          message.react("👍");
          message.channel.send(
            `**Prison Radio - Rap**\n${rapPlaylistt}\n\n**Prison Radio - All Rock**\n${allRockPlaylist}\n\n**Prison Radio - Country**\n${countryPlaylist}\n\n**Prison Radio - Friday Evening Playlist**\n${fridayeveningPlaylist}`
          );
        } else if (message.channel == musicCommandsChannel) {
          message.react("👍");
          message.channel.send(
            `**Prison Radio - Rap**\n${rapPlaylistt}\n\n**Prison Radio - All Rock**\n${allRockPlaylist}\n\n**Prison Radio - Country**\n${countryPlaylist}\n\n**Prison Radio - Friday Evening Playlist**\n${fridayeveningPlaylist}`
          );
        } else {
          return;
        }

        break;

      case "neww":
        if (message.member.hasPermission("MANAGE_NICKNAMES")) {
          // message.channel.send(chanceObj.pickone(newFeature));
          message.channel
            .send("This command has been disabled.")
            .then((m) => m.delete({ timeout: 5000 }));
        } else {
          return;
        }

        break;

      case "messagecount":
        const messageCountSchema = require("./database/schemas/messageCountSchema");

        message.channel.send("fetching results ...").then(async (msg) => {
          // console.log('it works!');
          const result = await messageCountSchema
            .find({})
            .sort({
              messageCount: -1,
            })
            .limit(5);
          // console.log(result);
          msg.edit(result);
          console.log(result);
        });
        break;

      case "clearthischannel":
        if (message.member.hasPermission("ADMINISTRATOR")) {
          message.react("👎");
          //   message.channel.send('This is a sensitive command. Please contact one of the Prison Officers to activate it.').then(msg => msg.delete({timeout: 10000}));
          message.channel.messages.fetch().then((result) => {
            message.channel.bulkDelete(result);
          });
        } else {
          message.react("👎");
          message.channel
            .send(
              "This is a sensitive command. Please contact one of the Prison Officers to activate it."
            )
            .then((msg) => msg.delete({ timeout: 10000 }));
          // return;
        }
        break;

      case "slow":
        if (message.member.hasPermission("MANAGE_NICKNAMES")) {
          if (message.channel.rateLimitPerUser > 30) {
            message.channel.setRateLimitPerUser(0);
            newinmatechannel.send(
              `${message.author} has deactivated slowmode for ${message.channel}.`
            );
          } else {
            message.channel.setRateLimitPerUser(120);
            newinmatechannel.send(
              `${message.author} has activated slowmode for ${message.channel}.`
            );
          }
          message.delete();
        }

        break;

      case "poll":
        const pollContent = message.content.replace("!poll ", "");

        if (message.member.hasPermission("MANAGE_NICKNAMES")) {
          message.channel.send(`${pollContent}`).then((msg) => {
            msg.react("🇦");
            msg.react("🅱️");
          });
        }
        message.delete();
        break;

      case "bday":
        if (message.author.bot) return;
        const birthdayCommandChannel = message.guild.channels.cache.get(
          "737398480233955438"
        );

        if (message.member.hasPermission("MANAGE_NICKNAMES")) {
          var person = message.mentions.members.first();
          if (!person)
            return message.channel
              .send("The command is `!bday @inmate set mmm-dd`")
              .then((m) => m.delete({ timeout: 5000 }))
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          const content = message.content.replace(`!bday `, ``);

          if (content.includes(" set ")) {
            if (content.includes("-")) {
              birthdayCommandChannel.send(
                "```" + `bb.override ${content}` + "```"
              );
              message.react("👍");
              message.channel.send(`I've noted down ${person}'s birthday.`);
            } else {
              message.react("👎");
              message.channel.send(
                "The command is `!bday @inmate set mmm-dd`. For example: `" +
                `!bday @${person.displayName} set jun-12` +
                "`."
              );
            }
          } else {
            message.react("👎");
            message.channel.send(
              "The command is `!bday @inmate set mmm-dd`. For example: `" +
              `!bday @${person.displayName} set jun-12` +
              "`."
            );
          }
        }

        break;

      case "ping":
        if (!message.member.hasPermission("MANAGE_NICKNAMES")) return;

        message.channel.send("calculating ping ...").then((resultMessage) => {
          const ping =
            resultMessage.createdTimestamp - message.createdTimestamp;

          resultMessage.delete();
          message.channel
            .send(
              "```Latency = " +
              ping +
              "ms``````API Latency = " +
              bot.ws.ping +
              "ms```"
            )
            .then((pingMessage) => {
              if (message.channel.id !== betatestChannel) {
                pingMessage.delete({ timeout: 5000 });
                message.delete({ timeout: 5000 });
              }
            });
        });

        break;

      case "parole":
        if (!message.member.hasPermission('MANAGE_NICKNAMES')) return;

        var person = message.mentions.members.first();
        if (!person)
          return message.channel
            .send("Please specify a valid user.")
            .then((m) => m.delete({ timeout: 5000 }))
            .then(
              message.delete({ timeout: 5000 }).catch((err) => console.log(err))
            );

        if (message.channel.parentID === parolecategory) {
          message.react("👎");
          message.delete({ timeout: 5000 });
          return;
        }

        if (person.id === bot.user.id)
          return message
            .react("👎")
            .then(
              message.delete({ timeout: 5000 }).catch((err) => console.log(err))
            );

        if (person.id === message.author.id)
          return message
            .react("👎")
            .then(
              message.delete({ timeout: 5000 }).catch((err) => console.log(err))
            );

        var inmaterole = person.guild.roles.cache.get("712289127948877844");
        var parolerole = person.guild.roles.cache.get("713007999009947648");

        if (message.member.hasPermission("MANAGE_NICKNAMES")) {
          if (person.hasPermission("MANAGE_NICKNAMES"))
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          const personParole = await messageCounterSchema.find({
            _id: person.id
          });

          if (personParole.length > 0) {
            const [{ paroleStatus, paroleChannelId }] = personParole

            if (paroleStatus === active) {
              message.channel.send(`${person} is already on parole.`).then(m => m.delete({ timeout: 10000 }));
              message.delete({ timeout: 10000 })
              return;
            }


            if (paroleChannelId) {
              const existingparoleChannel = message.guild.channels.cache.get(paroleChannelId);

              if (existingparoleChannel) {

                existingparoleChannel.setParent(parolecategory).then(async () => {
                  existingparoleChannel.overwritePermissions([
                    {
                      allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'ATTACH_FILES', 'EMBED_LINKS', 'ADD_REACTIONS', 'USE_EXTERNAL_EMOJIS', 'READ_MESSAGE_HISTORY'],
                      id: person
                    },
                    {
                      deny: 'VIEW_CHANNEL',
                      id: message.guild.id
                    },
                    {
                      allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'READ_MESSAGE_HISTORY', 'USE_EXTERNAL_EMOJIS', 'ATTACH_FILES', 'EMBED_LINKS', 'ADD_REACTIONS'],
                      id: officerRole
                    },
                    {
                      allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'READ_MESSAGE_HISTORY', 'USE_EXTERNAL_EMOJIS', 'ATTACH_FILES', 'EMBED_LINKS', 'ADD_REACTIONS'],
                      id: gateKeeperRole
                    }
                  ]);

                  existingparoleChannel.send(`${person}, you have been put on parole by ${message.author}. Please hang on till an officer is here to interrogate you.`);

                  await messageCounterSchema.findOneAndUpdate({
                    _id: person.id
                  }, {
                    paroleStatus: active,
                  }, {
                    upsert: true
                  })

                  person.roles.add(parolerole);
                  message.react("👍");
                  message.delete({ timeout: 5000 });
                  newinmatechannel.send(`${message.author} just put ${person} on parole, from ${message.channel}`)
                })
                return;

              } else {

                message.guild.channels.create(`${person.displayName} interrogation`, {
                  type: 'text',
                  parent: parolecategory,
                  position: 0,
                  permissionOverwrites: [
                    {
                      allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'ATTACH_FILES', 'EMBED_LINKS', 'ADD_REACTIONS', 'USE_EXTERNAL_EMOJIS', 'READ_MESSAGE_HISTORY'],
                      id: person
                    },
                    {
                      deny: 'VIEW_CHANNEL',
                      id: message.guild.id
                    },
                    {
                      allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'READ_MESSAGE_HISTORY', 'USE_EXTERNAL_EMOJIS', 'ATTACH_FILES', 'EMBED_LINKS', 'ADD_REACTIONS'],
                      id: officerRole
                    },
                    {
                      allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'READ_MESSAGE_HISTORY', 'USE_EXTERNAL_EMOJIS', 'ATTACH_FILES', 'EMBED_LINKS', 'ADD_REACTIONS'],
                      id: gateKeeperRole
                    }
                  ]
                }).then(async (paroleChannel) => {
                  paroleChannel.send(`${person}, you have been put on parole by ${message.author}. Please hang on till an officer is here to interrogate you.`);

                  await messageCounterSchema.findOneAndUpdate({
                    _id: person.id
                  }, {
                    paroleStatus: active,
                    paroleChannelId: paroleChannel.id
                  }, {
                    upsert: true
                  })

                  person.roles.add(parolerole);
                  message.react("👍");
                  message.delete({ timeout: 5000 });
                  newinmatechannel.send(`${message.author} just put ${person} on parole, from ${message.channel}`)
                })
                return;

              }
            } else {

              message.guild.channels.create(`${person.displayName} interrogation`, {
                type: 'text',
                parent: parolecategory,
                position: 0,
                permissionOverwrites: [
                  {
                    allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'ATTACH_FILES', 'EMBED_LINKS', 'ADD_REACTIONS', 'USE_EXTERNAL_EMOJIS', 'READ_MESSAGE_HISTORY'],
                    id: person
                  },
                  {
                    deny: 'VIEW_CHANNEL',
                    id: message.guild.id
                  },
                  {
                    allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'READ_MESSAGE_HISTORY', 'USE_EXTERNAL_EMOJIS', 'ATTACH_FILES', 'EMBED_LINKS', 'ADD_REACTIONS'],
                    id: officerRole
                  },
                  {
                    allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'READ_MESSAGE_HISTORY', 'USE_EXTERNAL_EMOJIS', 'ATTACH_FILES', 'EMBED_LINKS', 'ADD_REACTIONS'],
                    id: gateKeeperRole
                  }
                ]
              }).then(async (paroleChannel) => {
                paroleChannel.send(`${person}, you have been put on parole by ${message.author}. Please hang on till an officer is here to interrogate you.`);

                await messageCounterSchema.findOneAndUpdate({
                  _id: person.id
                }, {
                  paroleStatus: active,
                  paroleChannelId: paroleChannel.id
                }, {
                  upsert: true
                })

                person.roles.add(parolerole);
                message.react("👍");
                message.delete({ timeout: 5000 });
                newinmatechannel.send(`${message.author} just put ${person} on parole, from ${message.channel}`)
              })
            };

          }
        };

        break;

      case "permit":
        if (message.member.hasPermission("MANAGE_NICKNAMES")) {
          if (message.channel.parentID === parolecategory) {
            var person = message.mentions.members.first();
            if (!person)
              return message.channel
                .send("Please specify a valid user.")
                .then((m) => m.delete({ timeout: 5000 }))
                .then(
                  message
                    .delete({ timeout: 5000 })
                    .catch((err) => console.log(err))
                );

            if (person.id === bot.user.id)
              return message
                .react("👎")
                .then(
                  message
                    .delete({ timeout: 5000 })
                    .catch((err) => console.log(err))
                );

            if (person.id === message.author.id)
              return message
                .react("👎")
                .then(
                  message
                    .delete({ timeout: 5000 })
                    .catch((err) => console.log(err))
                );

            if (person.hasPermission("MANAGE_NICKNAMES"))
              return message
                .react("👎")
                .then(
                  message
                    .delete({ timeout: 5000 })
                    .catch((err) => console.log(err))
                );

            var inmaterole = person.guild.roles.cache.get("712289127948877844");
            var parolerole = person.guild.roles.cache.get("713007999009947648");
            let badgerole = person.guild.roles.cache.get("720213022076829728");

            message.channel.setParent(parolearchivecategory);
            message.channel.setPosition(0);

            await messageCounterSchema.findOneAndUpdate({
              _id: person.id
            }, {
              paroleStatus: inactive
            }, {
              upsert: true
            })

            person.roles.remove(parolerole);
            person.roles.add(inmaterole);
            person.roles.add(badgerole);
            message.react("👍");
            newinmatechannel.send(
              "<@" +
              message.author.id +
              "> just permitted <@" +
              person +
              "> to access the server."
            );
            // message.delete({timeout: 5000}).catch(err => console.log(err));
          }
        }

        if (message.channel == entrygatechannel) {
          var person = message.mentions.members.first();
          if (!person)
            return message.channel
              .send("Please specify a valid user.")
              .then((m) => m.delete({ timeout: 5000 }))
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.id === bot.user.id)
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.id === message.author.id)
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.hasPermission("MANAGE_NICKNAMES"))
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (message.member.hasPermission("MANAGE_NICKNAMES")) {
            var inmaterole = person.guild.roles.cache.get("712289127948877844");
            var parolerole = person.guild.roles.cache.get("713007999009947648");
            let badgerole = person.guild.roles.cache.get("720213022076829728");
            let newinmaterole = person.guild.roles.cache.get(
              "727664329222258730"
            );

            person.roles.remove(parolerole);
            person.roles.remove(newinmaterole);
            person.roles.add(inmaterole);
            person.roles.add(badgerole);
            message.react("👍");
            newinmatechannel.send(
              "<@" +
              message.author.id +
              "> just permitted <@" +
              person +
              "> to access the server."
            );
            welcomechannel.send(
              `Welcome to **Yaquta's Prison**, ${person}. I'm your warden. It looks like, you've committed a serious felony and it's about time to get you :lock:.  Now head to <#712368299891228784> and write a brief introduction about yourself. Also, don't forget to read the stuff in <#712285292366921781>. Once you're done with all that, join us in the <#712390649151881288> and meet your follow inmates.`
            );
            message.delete({ timeout: 5000 }).catch((err) => console.log(err));
          } else {
            message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );
          }
        }
        break;

      case "kick":
        if (message.channel == sentencechannel) {
          var person = message.mentions.members.first();
          if (!person)
            return message.channel
              .send("Please specify a valid user.")
              .then((m) => m.delete({ timeout: 5000 }))
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.id === bot.user.id)
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.id === message.author.id)
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.hasPermission("MANAGE_NICKNAMES"))
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          var inmaterole = person.guild.roles.cache.get("712289127948877844");
          var parolerole = person.guild.roles.cache.get("713007999009947648");

          if (message.member.hasPermission("KICK_MEMBERS")) {
            message.react("👍");
            person.kick("You have been kicked out of Yaquta's Prison Server");
            newinmatechannel.send(
              "<@" +
              message.author.id +
              "> has kicked <@" +
              person.id +
              "> from the server. "
            );
            message.delete({ timeout: 5000 }).catch((err) => console.log(err));
          } else {
            message.react("🗒️");
            message.channel
              .send(
                "Your requset to kick <@" +
                person.id +
                "> has been placed. In the meantime, I have placed the inmate on parole."
              )
              .then((m) => m.delete({ timeout: 10000 }))
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

            person.roles.add(parolerole);
            // person.roles.remove(inmaterole);

            newinmatechannel.send(
              "<@&712286690764652584>, <@" +
              message.author.id +
              "> has requested to kick <@" +
              person.id +
              "> from the server. "
            );
          }
        }

        if (message.channel == entrygatechannel) {
          var person = message.mentions.members.first();
          if (!person)
            return message.channel
              .send("Please specify a valid user.")
              .then((m) => m.delete({ timeout: 5000 }))
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.id === bot.user.id)
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.id === message.author.id)
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.hasPermission("MANAGE_NICKNAMES"))
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (message.member.hasPermission("MANAGE_NICKNAMES")) {
            var inmaterole = person.guild.roles.cache.get("712289127948877844");
            var parolerole = person.guild.roles.cache.get("713007999009947648");
            let newinmaterole = person.guild.roles.cache.get(
              "727664329222258730"
            );

            if (message.member.hasPermission("KICK_MEMBERS")) {
              person.kick("You have been kicked.");
              message.react("👍");
              newinmatechannel.send(
                "<@" +
                message.author.id +
                "> has kicked <@" +
                person.id +
                ">, from the server, upon entry."
              );
              message
                .delete({ timeout: 5000 })
                .catch((err) => console.log(err));
            } else {
              person.roles.remove(newinmaterole);
              message.react("👍");
              newinmatechannel
                .send(
                  "<@" +
                  message.author.id +
                  "> just requested to kick <@" +
                  person.id +
                  ">, from <#" +
                  entrygatechannel.id +
                  ">."
                )
                .then((m) => m.delete({ timeout: 10000 }))
                .then(
                  message
                    .delete({ timeout: 5000 })
                    .catch((err) => console.log(err))
                );
            }
          } else {
            message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );
          }
        }

        if (message.channel.parentID === parolecategory) {
          var person = message.mentions.members.first();
          if (!person)
            return message.channel
              .send("Please specify a valid user.")
              .then((m) => m.delete({ timeout: 5000 }))
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.id === bot.user.id)
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.id === message.author.id)
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.hasPermission("MANAGE_NICKNAMES"))
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          var inmaterole = person.guild.roles.cache.get("712289127948877844");
          var parolerole = person.guild.roles.cache.get("713007999009947648");

          if (message.member.hasPermission("MANAGE_NICKNAMES")) {
            if (message.member.hasPermission("KICK_MEMBERS")) {
              message.react("👍");
              person.kick("You have been kicked out of Yaquta's Prison Server");
              newinmatechannel.send(
                "<@" +
                message.author.id +
                "> has kicked <@" +
                person.id +
                "> from the server. "
              );
            } else {
              message.react("🗒️");
              message.channel
                .send(
                  "Your requset to kick <@" +
                  person.id +
                  "> has been placed. In the meantime, I have placed the inmate on parole."
                )
                .then((m) => m.delete({ timeout: 10000 }));
              newinmatechannel.send(
                "<@&712286690764652584>, <@" +
                message.author.id +
                "> has requested to kick <@" +
                person.id +
                "> from the server. "
              );
            }
          }

          message.channel.setParent(parolearchivecategory);
          message.channel.setPosition(0);
          message.channel.overwritePermissions([
            {
              deny: ["VIEW_CHANNEL", "SEND_MESSAGES"],
              id: message.guild.id,
            },
            {
              allow: ["VIEW_CHANNEL", "READ_MESSAGE_HISTORY"],
              id: officerRole,
            },
            {
              allow: ["VIEW_CHANNEL", "READ_MESSAGE_HISTORY"],
              id: gateKeeperRole,
            },
          ]);
        }

        break;

      case "ban":
        if (message.channel == sentencechannel) {
          var person = message.mentions.members.first();
          if (!person)
            return message.channel
              .send("Please specify a valid user.")
              .then((m) => m.delete({ timeout: 5000 }))
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.id === bot.user.id)
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.id === message.author.id)
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.hasPermission("MANAGE_NICKNAMES"))
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          var inmaterole = person.guild.roles.cache.get("712289127948877844");
          var parolerole = person.guild.roles.cache.get("713007999009947648");

          message.delete({ timeout: 5000 }).catch((err) => console.log(err));

          if (message.member.hasPermission("BAN_MEMBERS")) {
            message.react("👍");
            person.ban("You have been banned from Yaquta's Prison Server");
            newinmatechannel.send(
              "<@" +
              message.author.id +
              "> has banned <@" +
              person.id +
              "> from the server. "
            );
          } else {
            message.react("🗒️");
            message.channel
              .send(
                "Your requset to ban <@" +
                person.id +
                "> has been placed and in the meantime, I have placed the inmate on parole."
              )
              .then((m) => m.delete({ timeout: 10000 }))
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

            person.roles.add(parolerole);
            person.roles.remove(inmaterole);

            newinmatechannel.send(
              "<@&712286690764652584>, <@" +
              message.author.id +
              "> has requested to ban <@" +
              person.id +
              "> from the server.. "
            );
          }
        }

        if (message.channel.parentID === parolecategory) {
          var person = message.mentions.members.first();
          if (!person)
            return message.channel
              .send("Please specify a valid user.")
              .then((m) => m.delete({ timeout: 5000 }))
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.id === bot.user.id)
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.id === message.author.id)
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          if (person.hasPermission("MANAGE_NICKNAMES"))
            return message
              .react("👎")
              .then(
                message
                  .delete({ timeout: 5000 })
                  .catch((err) => console.log(err))
              );

          var inmaterole = person.guild.roles.cache.get("712289127948877844");
          var parolerole = person.guild.roles.cache.get("713007999009947648");

          if (message.member.hasPermission("MANAGE_NICKNAMES")) {
            if (message.member.hasPermission("BAN_MEMBERS")) {
              message.react("👍");
              person.ban("You have been banned from Yaquta's Prison Server");
              newinmatechannel.send(
                "<@" +
                message.author.id +
                "> has **banned** <@" +
                person.id +
                "> from the server. "
              );
            } else {
              message.react("🗒️");
              message.channel
                .send(
                  "Your requset to ban <@" + person.id + "> has been placed."
                )
                .then((m) => m.delete({ timeout: 10000 }));
              newinmatechannel.send(
                "<@&712286690764652584>, <@" +
                message.author.id +
                "> has requested to ban <@" +
                person.id +
                "> from the server. "
              );
            }
          }

          message.channel.setParent(parolearchivecategory);
          message.channel.setPosition(0);
          message.channel.overwritePermissions([
            {
              deny: ["VIEW_CHANNEL", "SEND_MESSAGES"],
              id: message.guild.id,
            },
            {
              allow: ["VIEW_CHANNEL", "READ_MESSAGE_HISTORY"],
              id: officerRole,
            },
            {
              allow: ["VIEW_CHANNEL", "READ_MESSAGE_HISTORY"],
              id: gateKeeperRole,
            },
          ]);
        }
        break;
    }
  }

  // @group commands
  if (message.content.startsWith(GROUPPREFIX)) {


    const groupCommandToLowercase = message.content.toLowerCase();


    if (message.channel === botCommandsChannel || message.channel === betatestChannel) {
      let args = message.content.substring(GROUPPREFIX.length).split(" ");
      switch (args[1]) {

        case "create":

          if (message.member.hasPermission('MANAGE_NICKNAMES')) {

            const groupCreateName = groupCommandToLowercase.replace(`${GROUPPREFIX} create `, '');

            if (groupCreateName.length < 1) return;

            // console.log(groupCreateName);

            // console.log(fetchGroup);

            const groupCreateSearch = await groupSchema.find({
              groupName: groupCreateName
            });

            if (groupCreateSearch.length < 1) {

              await groupSchema.findOneAndUpdate({
                groupName: groupCreateName
              }, {
                groupCreator: message.member.displayName,
                groupCreatorId: message.author.id,
                groupCreatedAt: message.createdAt.toLocaleString("en-IN", { timeZone: "Asia/Kolkata" }),
                groupStatus: active
              }, {
                upsert: true
              })

              message.react("👍");
              message.channel.send(`Created new group __${groupCreateName}__`);
            } else {
              message.reply(`${groupCreateName} already exists on this server.`);
              message.react('👎');
            };

          };
          return;

        case "join":

          const groupJoinName = messageContentLowercase.replace(`${GROUPPREFIX} join `, "");

          // console.log(groupJoinName);

          if (groupJoinName.length < 1) return;

          const groupJoinSearch = await groupSchema.find({
            groupName: groupJoinName
          });

          if (groupJoinSearch.length > 0) {

            const [{ groupStatus, groupName, groupMembers }] = groupJoinSearch;

            if (groupStatus === inactive) return message.reply(`the group ${groupName} is not functional anymore.`);

            // console.log(groupMembers);

            if (groupMembers.includes(message.author.id)) return message.reply('you are already a part of this group.');


            await groupSchema.findOneAndUpdate({
              groupName: groupJoinName
            }, {
              $addToSet: {
                groupMembers: message.member.id
              }
            })

            message.react("👍");
          } else {
            // message.reply(`that group does not exist on this server. Try "${GROUPPREFIX} search"`);
            message.reply(`that group does not exist on this server.`);
            message.react('👎');

            // ==========================

            const groupJoinSearch = await groupSchema.find({
              // groupName: groupCallName,
            });

            // console.log(groupJoinSearch);

            const groupJoinSearchOptions = {
              threshold: 0.4,
              keys: [
                "groupName"
              ]
            };

            const fuseSearch = new Fuse(groupJoinSearch, groupJoinSearchOptions);

            const fuseSearchResult = fuseSearch.search(groupJoinName);

            // console.log(fuseSearchResult)

            if (fuseSearchResult.length > 0) {

              const [{ item }] = fuseSearchResult
              const { groupName: fuseGoupName, groupMembers: fuseGroupMember, groupStatus: fuseGroupStatus } = item;

              message.channel.send(`Did you mean **${fuseGoupName}**? ` + "If yes, then try ```@group join " + fuseGoupName + "```");

            } else {
              return;
            }

            // ==========================

          }
          return;

        case "leave":

          const groupLeaveName = messageContentLowercase.replace(`${GROUPPREFIX} leave `, "");

          // console.log(groupLeaveName);

          if (groupLeaveName.length < 1) return;

          const groupLeaveSearch = await groupSchema.find({
            groupName: groupLeaveName
          });

          if (groupLeaveSearch.length > 0) {

            const [{ groupStatus, groupName, groupMembers }] = groupLeaveSearch;

            if (groupStatus === inactive) return message.react(`the group ${groupName} is not functional anymore.`);

            if (!groupMembers.includes(message.author.id)) return message.reply('you are already not a part of this group.');

            await groupSchema.findOneAndUpdate({
              groupName: groupLeaveName
            }, {
              $pull: {
                groupMembers: message.member.id
              }
            })

            message.react("👍");
          } else {
            // message.reply(`that group does not exist on this server. Try "${GROUPPREFIX} search"`);
            message.reply(`that group does not exist on this server.`);
            message.react('👎');

            // ==========================

            const groupLeaveSearch = await groupSchema.find({
              // groupName: groupCallName,
            });

            // console.log(groupLeaveSearch);

            const groupLeaveSearchOptions = {
              threshold: 0.4,
              keys: [
                "groupName"
              ]
            };

            const fuseSearch = new Fuse(groupLeaveSearch, groupLeaveSearchOptions);

            const fuseSearchResult = fuseSearch.search(groupLeaveName);

            // console.log(fuseSearchResult)

            if (fuseSearchResult.length > 0) {

              const [{ item }] = fuseSearchResult
              const { groupName: fuseGoupName, groupMembers: fuseGroupMember, groupStatus: fuseGroupStatus } = item;

              message.channel.send(`Did you mean **${fuseGoupName}**? ` + "If yes, then try ```@group leave " + fuseGoupName + "```");

            } else {
              return;
            }

            // ==========================

          }
          return;

        // case 'search':

        //   const groupSearch = await groupSchema.find({
        //     groupStatus: active
        //   });

        //   const [{ groupName, groupStatus }] = groupSearch

        //   console.log(groupSearch)

        //   var groups = "";
        //   for (i in groupName) {
        //     groups +=  groupName[i] + "\n";
        //   }
        //   message.channel.send(`${groups}`);

        //   return;

        case 'activate':

          if (message.member.hasPermission('MANAGE_NICKNAMES')) {

            const groupActivateName = messageContentLowercase.replace(`${GROUPPREFIX} activate `, "");

            const groupActiveSearch = await groupSchema.find({
              groupName: groupActivateName
            });

            if (groupActiveSearch.length > 0) {

              const [{ groupStatus, groupName, groupMembers }] = groupActiveSearch;

              if (groupStatus === active) return message.reply('this group is already active.');

              await groupSchema.findOneAndUpdate({
                groupName
              }, {
                groupStatus: active
              }).then(() => {
                message.react("👍");
                message.reply(`${groupName} is now ${active} with ${groupMembers.length} members.`);
              })

            } else {
              message.reply('I cannot find that group.');

              // ==========================

              const groupActivate = await groupSchema.find({});


              const groupActivateOptions = {
                threshold: 0.4,
                keys: [
                  "groupName"
                ]
              };

              const fuseSearch = new Fuse(groupActivate, groupActivateOptions);

              const fuseSearchResult = fuseSearch.search(groupActivateName);

              // console.log(fuseSearchResult)

              if (fuseSearchResult.length > 0) {

                const [{ item }] = fuseSearchResult
                const { groupName: fuseGoupName, groupMembers: fuseGroupMember, groupStatus: fuseGroupStatus } = item;

                message.channel.send(`Did you mean **${fuseGoupName}**? ` + "If yes, then try ```@group activate " + fuseGoupName + "```");

              } else {
                return;
              }

              // ==========================
            }

          }

          return;

        case 'deactivate':

          if (message.member.hasPermission('MANAGE_NICKNAMES')) {

            const groupDeactivateName = messageContentLowercase.replace(`${GROUPPREFIX} deactivate `, "");

            const groupInactiveSearch = await groupSchema.find({
              groupName: groupDeactivateName
            });

            if (groupInactiveSearch.length > 0) {

              const [{ groupStatus, groupName, groupMembers }] = groupInactiveSearch;

              if (groupStatus === inactive) return message.reply('this group is already inactive.');

              await groupSchema.findOneAndUpdate({
                groupName
              }, {
                groupStatus: inactive
              }).then(() => {
                message.react("👍");
                message.reply(`${groupName} is now ${inactive} with ${groupMembers.length} members.`);
              })

            } else {
              message.reply('I cannot find that group.');

              // ==========================

              const groupDeactivate = await groupSchema.find({});


              const groupDeactivateOptions = {
                threshold: 0.4,
                keys: [
                  "groupName"
                ]
              };

              const fuseSearch = new Fuse(groupDeactivate, groupDeactivateOptions);

              const fuseSearchResult = fuseSearch.search(groupDeactivateName);

              // console.log(fuseSearchResult)

              if (fuseSearchResult.length > 0) {

                const [{ item }] = fuseSearchResult
                const { groupName: fuseGoupName, groupMembers: fuseGroupMember, groupStatus: fuseGroupStatus } = item;

                message.channel.send(`Did you mean **${fuseGoupName}**? ` + "If yes, then try ```@group deactivate " + fuseGoupName + "```");

              } else {
                return;
              }

              // ==========================
            }

          }

          return;


      }
    }

    // calling the group

    const groupCallName = messageContentLowercase.replace(`${GROUPPREFIX} `, "");

    const groupCallSearch = await groupSchema.find({
      groupName: groupCallName,
    });



    if (message.channel === chatChannel || message.channel === blockChat || message.channel === betatestChannel) {

      if (groupCallSearch.length > 0) {

        const [{ groupName, groupMembers, groupStatus }] = groupCallSearch

        if (groupStatus === inactive) return;

        // calls
        if (message.member.hasPermission('MANAGE_NICKNAMES')) {

          if (groupCallSet.has(groupName)) {

            var groupCalls = "";
            for (i in groupMembers) {
              groupCalls += "<@!" + groupMembers[i] + "> ";
            }
            message.channel.send(`**@${groupName}:** ${groupCalls}`);
            // message.channel.send(`**@${groupName}:**` + "```" + `${groupCalls}` + "```"); // tsst

            groupCallSet.delete(groupName);
            groupCallSet.add(groupName);
            setTimeout(() => {
              groupCallSet.delete(groupName);
            }, 1800000);

          } else {

            var groupCalls = "";
            for (i in groupMembers) {
              groupCalls += "<@!" + groupMembers[i] + "> ";
            }
            message.channel.send(`**@${groupName}:** ${groupCalls}`);
            // message.channel.send(`**@${groupName}:**` + "```" + `${groupCalls}` + "```"); // tsst

            groupCallSet.add(groupName);
            setTimeout(() => {
              groupCallSet.delete(groupName);
            }, 1800000);

          }

        } else {

          if (!groupCallerSet.has(message.author.id)) {

            if (!groupCallSet.has(groupName)) {
              var groupCalls = "";
              for (i in groupMembers) {
                groupCalls += "<@!" + groupMembers[i] + "> ";
              }
              message.channel.send(`**@${groupName}:** ${groupCalls}`);
              // message.channel.send(`**@${groupName}:**` + "```" + `${groupCalls}` + "```"); // tsst

              groupCallSet.add(groupName);
              groupCallerSet.add(message.author.id);
              setTimeout(() => {
                groupCallSet.delete(groupName);
                groupCallerSet.delete(message.author.id);
              }, 1800000);
            } else {
              message.reply(`this group was pinged in the past half an hour. Try again in a while.`).then(m => m.delete({ timeout: 10000 }));
              message.delete();
            }

          } else {
            return message.reply(`hold on sport! You just pinged a group. You can try that again in a while.`);
          }
        }
      } else {
        return;
      }

    } else {
      return;
    }
  }

  // Birthday notification
  if (message.channel == bdayNotifChannel) {
    var birthdayPerson = message.mentions.members.first();
    var birthdayWish = chanceObj.pickone([
      `HOOORRRAAAYYY! It's ${birthdayPerson}'s birthday!🎉 🎊`,
      `OMG! Today is ${birthdayPerson}'s birthday!! 🎉 🎊`,
      `EVERYONE! It's ${birthdayPerson}'s birthday!!🎉 🎊`,
    ])

    if (message.author.bot && birthdayPerson) {
      chatChannel.send(`**${birthdayWish}**`);
    } else {
      return;
    }
  };

  // Spam mesages in team chats
  if (usersMap.has(message.author.id)) {


    const randomSpamAlert = chanceObj.pickone([
      'that is spam!',
      'can you not spam?',
      'SPAMMER'
    ])

    const userData = usersMap.get(message.author.id);
    const { lastMessage, timer } = userData;
    const difference = message.createdTimestamp - lastMessage.createdTimestamp;
    let msgCount = userData.msgCount;
    // console.log(difference);
    if (difference > DIFF) {
      clearTimeout(timer);
      // console.log('Cleared timeout');
      userData.msgCount = 1;
      userData.lastMessage = message;
      userData.timer = setTimeout(() => {
        usersMap.delete(message.author.id);
        // console.log('Removed from RESET.');
      }, TIME);
      usersMap.set(message.author.id, userData);
    }
    else {
      ++msgCount;
      if (parseInt(msgCount) === LIMIT) {
        const spamRole = message.guild.roles.cache.get('713007999009947648');
        message.member.roles.add(spamRole);
        message.channel.send(`${message.author}, ${randomSpamAlert}`);
        newinmatechannel.send(`${message.author} is in temporary parole for spamming.`)
        setTimeout(() => {
          message.member.roles.remove(spamRole);
        }, TIME);
      } else {
        userData.msgCount = msgCount;
        usersMap.set(message.author.id, userData);
      }
    }
  }
  else {
    if (message.author.bot) return;
    let fn = setTimeout(() => {
      usersMap.delete(message.author.id);
      // console.log('Removed from map.');
    }, TIME);
    usersMap.set(message.author.id, {
      msgCount: 1,
      lastMessage: message,
      timer: fn
    });
  }

  // robo question
  if (message.channel == blockchat || message.channel == chatChannel) {
    if (messageContentLowercase.startsWith("robo ")) {
      if (message.content.endsWith("?")) {
        message.reply(
          chanceObj.pickone([
            "yes.",
            "doubtful.",
            "probably.",
            "definitely.",
            "not a chance",
            "absolutely not.",
          ])
        );
      }
    }
  };

  // joy emote
  if (message.channel == betatestChannel) {
    if (message.content.includes("😂")) {
      message.channel.send("||beef||");
    }
  }

  // discord link deletion
  if (message.content.includes(`https://discord.gg/`)) {
    message.delete();
  };

  //
  // robo joke
  if (message.channel == riddleChannel) {
    if (messageContentLowercase.startsWith("robo ")) {
      if (messageContentLowercase.endsWith("joke")) {
        message.channel.startTyping();
        const joke = await fetch("http://official-joke-api.appspot.com/random_joke").then((response) => response.json());

        message.channel.send(`**Setup**\n${joke.setup}\n\n**Punchline**\n${joke.punchline}`).then(() => { message.channel.stopTyping(); });
      }
    }
  }

  // Reply to @warden
  //<@&712286690764652584> - warden
  //<@&712768970805084190> - beta
  //<@!712289127948877844> - Inmate
  if (message.content.includes("<@&712286690764652584>")) {
    if (message.channel === officechannel) return;
    if (message.channel === headofficechannel) return;
    if (message.member.hasPermission('ADMINISTRATOR')) return;
    message.reply("one of the wardens will get back with you, soon. Hang on!");
  }

  //Deleting chats in logs channel
  const logsChannel = message.guild.channels.cache.get("718779866710933615");
  if (message.channel == logsChannel) {
    message.delete({ timeout: 1000 }).catch((err) => console.log(err));
    //600000 = 10 min
  }

  // Greetins message delete after 10 minutes
  // if(message.guild.id !== '711634547770654791') return;
  const greetingsChannel = message.guild.channels.cache.get("715340560826630174");

  if (message.channel == greetingsChannel) {
    message.delete({ timeout: 300000 }).catch((err) => console.log(err));
    //600000 = 10 min
  }

});

// bot.on('channelPinsUpdate', (oldPresence, newPresence) => {

// })

//Reply to @robowarden and @support
// bot.on('message', message => {
// if (message.content.includes('<@!712970953298149426>')) {
// if (message.author.bot) return;
// if (message.member.hasPermission('KICK_MEMBERS')) return;
// message.channel.send('Talking to someone was not a part of my job description. You can always call a warden with `@Warden 👮`, if you think something is up.');
// };
// if (message.content.includes('<@&734901488509255702>')) {
// if (message.author.bot) return;
// if (message.member.hasPermission('KICK_MEMBERS')) return;
// message.reply("I'm summoning a <@&712286690764652584> for you.");
// };
// });

databaseImport(bot);

// // BRO - delete
// bot.on('message', message => {
//     if (message.content.toLowerCase() === 'bro'.toLocaleLowerCase() ) {
//     message.delete({timeout: 1000}).catch(err => console.log(err));
//     }
//     });

//   // I SAW THAT for delete
// bot.on('messageDelete', message => {
//   if (message.author.bot) return;
//   message.channel.send('<:sawthat:717450204806250557>');
//   }
// );
//   bot.on('message', message =>{
//     if(message.author.bot){
//       if (message.content.includes('<:sawthat:717450204806250557>')){
//       message.delete({timeout: 2000}).catch(err => console.log(err));
//       }
//   }
// });

// basic message reaction
const basicMessageReactions = require("./events/basicMessageReactions.js");
basicMessageReactions(bot);

//Message update logging
bot.on("messageUpdate", async (oldMessage, newMessage) => {
  const backupUpdateLogs = bot.channels.cache.get("754175670522871849");

  if (oldMessage.author.bot) return;
  if (oldMessage.content === newMessage.content) {
    return;
  }

  if (newMessage.content.includes(`https://discord.gg/`) || oldMessage.content.includes(`https://discord.gg/`)) {
    message.delete();
    return;
  };

  let logembed = new Discord.MessageEmbed()
    .setThumbnail(oldMessage.author.avatarURL())
    .setAuthor("Message Edited")
    .setColor("#FFFF00")
    .setDescription(`<@${oldMessage.author.id}>`)
    .addField("Channel", `<#${newMessage.channel.id}>`, true)
    .addField("Before", oldMessage.content, true)
    .addField("After", newMessage.content, true)
    .addField("Sent at", oldMessage.createdAt.toLocaleString("en-IN", { timeZone: "Asia/Kolkata", }) + " (IST)", true)
    .addField("Message Link", `https://discordapp.com/channels/711634547770654791/${newMessage.channel.id}/${newMessage.id}`)
    .setTimestamp();

  let loggingChannel = newMessage.guild.channels.cache.get("718779866710933615");
  if (!loggingChannel) return;
  if (!backupUpdateLogs) return;

  loggingChannel.send(logembed);
  backupUpdateLogs.send(logembed);
});

//Deleted messages logging
bot.on("messageDelete", async (message) => {
  const backupDelLogs = bot.channels.cache.get("754175670522871849");

  if (message.author.bot) return;
  // const CHANNEL = message.guild.channels.cache.get('718779866710933615');
  const sentencechan = message.guild.channels.cache.get("727551487915327488");
  if (message.channel == sentencechan) return;
  // if (Message.channel == CHANNEL)
  // if (Message.author.id === "712970953298149426") return;
  let logDelembed = new Discord.MessageEmbed()
    .setThumbnail(message.author.avatarURL())
    .setAuthor("Message Deleted")
    .setColor("#FF0000")
    .setDescription("<@" + message.author.id + ">")
    .addField("Channel", "<#" + message.channel.id + ">", true)
    .addField("Message", message.content || "image", true)
    // .addField("Sent". message.createdAt.toLocaleString("en-IN", {timeZone: "Asia/Kolkata"}) + " (IST)", true)
    .setTimestamp();

  let loggingDelChannel = message.guild.channels.cache.get(
    "718779866710933615"
  );
  if (!loggingDelChannel) return;
  if (!backupDelLogs) return;

  loggingDelChannel.send(logDelembed);
  backupDelLogs.send(logDelembed);
});

// n word warning
const nwordfilters = require("./events/n-word filter.js");
nwordfilters(bot);

//                      BETA Zone

// bot.on('presenceUpdate')

// Scaling twins channel(`🎤 Inmates C-Block`) Voice Channel
const scalingChannels = require("./events/scalingVoiceChannels.js");
scalingChannels(bot);

const nitroRoles = require('./src/collections/nitronooster.json');
const messageCountSchema = require("./database/schemas/messageCountSchema");
const groupSchema = require("./database/schemas/groupSchema");
const movieNightsSchema = require('./database/schemas/movieNightsSchema');


bot.on('messageReactionAdd', async (reaction, user) => {
  const movienightsChannel = reaction.message.guild.channels.cache.get('715255733666709554');

  if (reaction.message.partial) await reaction.message.fetch();
  if (reaction.partial) await reaction.fetch();
  if (user.bot) return;

  if (reaction.message.id === '761643037255401493') {
    // console.log(reaction.message.member.roles.cache.keyArray());
    // if(reaction.message.member.roles.cache.has(warden)) {
    //   console.log('worked');
    // }

    if (reaction.emoji.name === nitroRoles.canaryYellow.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.add(nitroRoles.canaryYellow.role);

      const colour1 = reaction.message.reactions.cache.get(nitroRoles.electricYellow.emoji.id)
      const colour2 = reaction.message.reactions.cache.get(nitroRoles.limeGreen.emoji.id)
      const colour3 = reaction.message.reactions.cache.get(nitroRoles.charteuseGreen.emoji.id)
      const colour4 = reaction.message.reactions.cache.get(nitroRoles.springGreen.emoji.id)
      const colour5 = reaction.message.reactions.cache.get(nitroRoles.irisPurple.emoji.id)
      const colour6 = reaction.message.reactions.cache.get(nitroRoles.ultraPink.emoji.id)
      const colour7 = reaction.message.reactions.cache.get(nitroRoles.flamingoRed.emoji.id)
      const colour8 = reaction.message.reactions.cache.get(nitroRoles.bubblegumPink.emoji.id)
      const colour9 = reaction.message.reactions.cache.get(nitroRoles.babyBlue.emoji.id)

      colour1.users.remove(user.id);
      colour2.users.remove(user.id);
      colour3.users.remove(user.id);
      colour4.users.remove(user.id);
      colour5.users.remove(user.id);
      colour6.users.remove(user.id);
      colour7.users.remove(user.id);
      colour8.users.remove(user.id);
      colour9.users.remove(user.id);

      return;
    }


    if (reaction.emoji.name === nitroRoles.electricYellow.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.add(nitroRoles.electricYellow.role);

      const colour1 = reaction.message.reactions.cache.get(nitroRoles.canaryYellow.emoji.id)
      const colour2 = reaction.message.reactions.cache.get(nitroRoles.limeGreen.emoji.id)
      const colour3 = reaction.message.reactions.cache.get(nitroRoles.charteuseGreen.emoji.id)
      const colour4 = reaction.message.reactions.cache.get(nitroRoles.springGreen.emoji.id)
      const colour5 = reaction.message.reactions.cache.get(nitroRoles.irisPurple.emoji.id)
      const colour6 = reaction.message.reactions.cache.get(nitroRoles.ultraPink.emoji.id)
      const colour7 = reaction.message.reactions.cache.get(nitroRoles.flamingoRed.emoji.id)
      const colour8 = reaction.message.reactions.cache.get(nitroRoles.bubblegumPink.emoji.id)
      const colour9 = reaction.message.reactions.cache.get(nitroRoles.babyBlue.emoji.id)

      colour1.users.remove(user.id);
      colour2.users.remove(user.id);
      colour3.users.remove(user.id);
      colour4.users.remove(user.id);
      colour5.users.remove(user.id);
      colour6.users.remove(user.id);
      colour7.users.remove(user.id);
      colour8.users.remove(user.id);
      colour9.users.remove(user.id);

      return;
    }

    if (reaction.emoji.name === nitroRoles.limeGreen.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.add(nitroRoles.limeGreen.role);

      const colour1 = reaction.message.reactions.cache.get(nitroRoles.electricYellow.emoji.id)
      const colour2 = reaction.message.reactions.cache.get(nitroRoles.canaryYellow.emoji.id)
      const colour3 = reaction.message.reactions.cache.get(nitroRoles.charteuseGreen.emoji.id)
      const colour4 = reaction.message.reactions.cache.get(nitroRoles.springGreen.emoji.id)
      const colour5 = reaction.message.reactions.cache.get(nitroRoles.irisPurple.emoji.id)
      const colour6 = reaction.message.reactions.cache.get(nitroRoles.ultraPink.emoji.id)
      const colour7 = reaction.message.reactions.cache.get(nitroRoles.flamingoRed.emoji.id)
      const colour8 = reaction.message.reactions.cache.get(nitroRoles.bubblegumPink.emoji.id)
      const colour9 = reaction.message.reactions.cache.get(nitroRoles.babyBlue.emoji.id)

      colour1.users.remove(user.id);
      colour2.users.remove(user.id);
      colour3.users.remove(user.id);
      colour4.users.remove(user.id);
      colour5.users.remove(user.id);
      colour6.users.remove(user.id);
      colour7.users.remove(user.id);
      colour8.users.remove(user.id);
      colour9.users.remove(user.id);

      return;
    }

    if (reaction.emoji.name === nitroRoles.charteuseGreen.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.add(nitroRoles.charteuseGreen.role);

      const colour1 = reaction.message.reactions.cache.get(nitroRoles.electricYellow.emoji.id)
      const colour2 = reaction.message.reactions.cache.get(nitroRoles.limeGreen.emoji.id)
      const colour3 = reaction.message.reactions.cache.get(nitroRoles.canaryYellow.emoji.id)
      const colour4 = reaction.message.reactions.cache.get(nitroRoles.springGreen.emoji.id)
      const colour5 = reaction.message.reactions.cache.get(nitroRoles.irisPurple.emoji.id)
      const colour6 = reaction.message.reactions.cache.get(nitroRoles.ultraPink.emoji.id)
      const colour7 = reaction.message.reactions.cache.get(nitroRoles.flamingoRed.emoji.id)
      const colour8 = reaction.message.reactions.cache.get(nitroRoles.bubblegumPink.emoji.id)
      const colour9 = reaction.message.reactions.cache.get(nitroRoles.babyBlue.emoji.id)

      colour1.users.remove(user.id);
      colour2.users.remove(user.id);
      colour3.users.remove(user.id);
      colour4.users.remove(user.id);
      colour5.users.remove(user.id);
      colour6.users.remove(user.id);
      colour7.users.remove(user.id);
      colour8.users.remove(user.id);
      colour9.users.remove(user.id);

      return;
    }

    if (reaction.emoji.name === nitroRoles.springGreen.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.add(nitroRoles.springGreen.role);

      const colour1 = reaction.message.reactions.cache.get(nitroRoles.electricYellow.emoji.id)
      const colour2 = reaction.message.reactions.cache.get(nitroRoles.limeGreen.emoji.id)
      const colour3 = reaction.message.reactions.cache.get(nitroRoles.charteuseGreen.emoji.id)
      const colour4 = reaction.message.reactions.cache.get(nitroRoles.canaryYellow.emoji.id)
      const colour5 = reaction.message.reactions.cache.get(nitroRoles.irisPurple.emoji.id)
      const colour6 = reaction.message.reactions.cache.get(nitroRoles.ultraPink.emoji.id)
      const colour7 = reaction.message.reactions.cache.get(nitroRoles.flamingoRed.emoji.id)
      const colour8 = reaction.message.reactions.cache.get(nitroRoles.bubblegumPink.emoji.id)
      const colour9 = reaction.message.reactions.cache.get(nitroRoles.babyBlue.emoji.id)

      colour1.users.remove(user.id);
      colour2.users.remove(user.id);
      colour3.users.remove(user.id);
      colour4.users.remove(user.id);
      colour5.users.remove(user.id);
      colour6.users.remove(user.id);
      colour7.users.remove(user.id);
      colour8.users.remove(user.id);
      colour9.users.remove(user.id);

      return;
    }

    if (reaction.emoji.name === nitroRoles.irisPurple.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.add(nitroRoles.irisPurple.role);

      const colour1 = reaction.message.reactions.cache.get(nitroRoles.electricYellow.emoji.id)
      const colour2 = reaction.message.reactions.cache.get(nitroRoles.limeGreen.emoji.id)
      const colour3 = reaction.message.reactions.cache.get(nitroRoles.charteuseGreen.emoji.id)
      const colour4 = reaction.message.reactions.cache.get(nitroRoles.springGreen.emoji.id)
      const colour5 = reaction.message.reactions.cache.get(nitroRoles.canaryYellow.emoji.id)
      const colour6 = reaction.message.reactions.cache.get(nitroRoles.ultraPink.emoji.id)
      const colour7 = reaction.message.reactions.cache.get(nitroRoles.flamingoRed.emoji.id)
      const colour8 = reaction.message.reactions.cache.get(nitroRoles.bubblegumPink.emoji.id)
      const colour9 = reaction.message.reactions.cache.get(nitroRoles.babyBlue.emoji.id)

      colour1.users.remove(user.id);
      colour2.users.remove(user.id);
      colour3.users.remove(user.id);
      colour4.users.remove(user.id);
      colour5.users.remove(user.id);
      colour6.users.remove(user.id);
      colour7.users.remove(user.id);
      colour8.users.remove(user.id);
      colour9.users.remove(user.id);

      return;
    }

    if (reaction.emoji.name === nitroRoles.ultraPink.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.add(nitroRoles.ultraPink.role);

      const colour1 = reaction.message.reactions.cache.get(nitroRoles.electricYellow.emoji.id)
      const colour2 = reaction.message.reactions.cache.get(nitroRoles.limeGreen.emoji.id)
      const colour3 = reaction.message.reactions.cache.get(nitroRoles.charteuseGreen.emoji.id)
      const colour4 = reaction.message.reactions.cache.get(nitroRoles.springGreen.emoji.id)
      const colour5 = reaction.message.reactions.cache.get(nitroRoles.irisPurple.emoji.id)
      const colour6 = reaction.message.reactions.cache.get(nitroRoles.canaryYellow.emoji.id)
      const colour7 = reaction.message.reactions.cache.get(nitroRoles.flamingoRed.emoji.id)
      const colour8 = reaction.message.reactions.cache.get(nitroRoles.bubblegumPink.emoji.id)
      const colour9 = reaction.message.reactions.cache.get(nitroRoles.babyBlue.emoji.id)

      colour1.users.remove(user.id);
      colour2.users.remove(user.id);
      colour3.users.remove(user.id);
      colour4.users.remove(user.id);
      colour5.users.remove(user.id);
      colour6.users.remove(user.id);
      colour7.users.remove(user.id);
      colour8.users.remove(user.id);
      colour9.users.remove(user.id);

      return;
    }

    if (reaction.emoji.name === nitroRoles.flamingoRed.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.add(nitroRoles.flamingoRed.role);

      const colour1 = reaction.message.reactions.cache.get(nitroRoles.electricYellow.emoji.id)
      const colour2 = reaction.message.reactions.cache.get(nitroRoles.limeGreen.emoji.id)
      const colour3 = reaction.message.reactions.cache.get(nitroRoles.charteuseGreen.emoji.id)
      const colour4 = reaction.message.reactions.cache.get(nitroRoles.springGreen.emoji.id)
      const colour5 = reaction.message.reactions.cache.get(nitroRoles.irisPurple.emoji.id)
      const colour6 = reaction.message.reactions.cache.get(nitroRoles.ultraPink.emoji.id)
      const colour7 = reaction.message.reactions.cache.get(nitroRoles.canaryYellow.emoji.id)
      const colour8 = reaction.message.reactions.cache.get(nitroRoles.bubblegumPink.emoji.id)
      const colour9 = reaction.message.reactions.cache.get(nitroRoles.babyBlue.emoji.id)

      colour1.users.remove(user.id);
      colour2.users.remove(user.id);
      colour3.users.remove(user.id);
      colour4.users.remove(user.id);
      colour5.users.remove(user.id);
      colour6.users.remove(user.id);
      colour7.users.remove(user.id);
      colour8.users.remove(user.id);
      colour9.users.remove(user.id);

      return;
    }

    if (reaction.emoji.name === nitroRoles.bubblegumPink.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.add(nitroRoles.bubblegumPink.role);

      const colour1 = reaction.message.reactions.cache.get(nitroRoles.electricYellow.emoji.id)
      const colour2 = reaction.message.reactions.cache.get(nitroRoles.limeGreen.emoji.id)
      const colour3 = reaction.message.reactions.cache.get(nitroRoles.charteuseGreen.emoji.id)
      const colour4 = reaction.message.reactions.cache.get(nitroRoles.springGreen.emoji.id)
      const colour5 = reaction.message.reactions.cache.get(nitroRoles.irisPurple.emoji.id)
      const colour6 = reaction.message.reactions.cache.get(nitroRoles.ultraPink.emoji.id)
      const colour7 = reaction.message.reactions.cache.get(nitroRoles.flamingoRed.emoji.id)
      const colour8 = reaction.message.reactions.cache.get(nitroRoles.canaryYellow.emoji.id)
      const colour9 = reaction.message.reactions.cache.get(nitroRoles.babyBlue.emoji.id)

      colour1.users.remove(user.id);
      colour2.users.remove(user.id);
      colour3.users.remove(user.id);
      colour4.users.remove(user.id);
      colour5.users.remove(user.id);
      colour6.users.remove(user.id);
      colour7.users.remove(user.id);
      colour8.users.remove(user.id);
      colour9.users.remove(user.id);

      return;
    }

    if (reaction.emoji.name === nitroRoles.babyBlue.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.add(nitroRoles.babyBlue.role);

      const colour1 = reaction.message.reactions.cache.get(nitroRoles.electricYellow.emoji.id)
      const colour2 = reaction.message.reactions.cache.get(nitroRoles.limeGreen.emoji.id)
      const colour3 = reaction.message.reactions.cache.get(nitroRoles.charteuseGreen.emoji.id)
      const colour4 = reaction.message.reactions.cache.get(nitroRoles.springGreen.emoji.id)
      const colour5 = reaction.message.reactions.cache.get(nitroRoles.irisPurple.emoji.id)
      const colour6 = reaction.message.reactions.cache.get(nitroRoles.ultraPink.emoji.id)
      const colour7 = reaction.message.reactions.cache.get(nitroRoles.flamingoRed.emoji.id)
      const colour8 = reaction.message.reactions.cache.get(nitroRoles.bubblegumPink.emoji.id)
      const colour9 = reaction.message.reactions.cache.get(nitroRoles.canaryYellow.emoji.id)

      colour1.users.remove(user.id);
      colour2.users.remove(user.id);
      colour3.users.remove(user.id);
      colour4.users.remove(user.id);
      colour5.users.remove(user.id);
      colour6.users.remove(user.id);
      colour7.users.remove(user.id);
      colour8.users.remove(user.id);
      colour9.users.remove(user.id);

      return;
    }

  }

  if (reaction.message.channel === movienightsChannel) {
    if (reaction.emoji.name === '✅') {

      await movieNightsSchema.findOneAndUpdate({
        _id: reaction.message.id
      }, {
        $addToSet: {
          reactedUserIds: user.id
        }
      }, {
        upsert: true
      })
    }
  }
})

bot.on('messageReactionRemove', async (reaction, user) => {

  const movienightsChannel = reaction.message.guild.channels.cache.get('715255733666709554');


  if (reaction.message.partial) await reaction.message.fetch();
  if (reaction.partial) await reaction.fetch();
  if (user.bot) return;

  if (reaction.message.id === '761643037255401493') {

    if (reaction.emoji.name === nitroRoles.canaryYellow.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.remove(nitroRoles.canaryYellow.role);
      return;
    }

    if (reaction.emoji.name === nitroRoles.electricYellow.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.remove(nitroRoles.electricYellow.role);
      return;
    }

    if (reaction.emoji.name === nitroRoles.limeGreen.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.remove(nitroRoles.limeGreen.role);
      return;
    }

    if (reaction.emoji.name === nitroRoles.charteuseGreen.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.remove(nitroRoles.charteuseGreen.role);
      return;
    }

    if (reaction.emoji.name === nitroRoles.springGreen.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.remove(nitroRoles.springGreen.role);
      return;
    }

    if (reaction.emoji.name === nitroRoles.irisPurple.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.remove(nitroRoles.irisPurple.role);
      return;
    }

    if (reaction.emoji.name === nitroRoles.ultraPink.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.remove(nitroRoles.ultraPink.role);
      return;
    }

    if (reaction.emoji.name === nitroRoles.flamingoRed.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.remove(nitroRoles.flamingoRed.role);
      return;
    }

    if (reaction.emoji.name === nitroRoles.bubblegumPink.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.remove(nitroRoles.bubblegumPink.role);
      return;
    }

    if (reaction.emoji.name === nitroRoles.babyBlue.emoji.name) {
      await reaction.message.guild.members.cache.get(user.id).roles.remove(nitroRoles.babyBlue.role);
      return;
    }

  }

  if (reaction.message.channel === movienightsChannel) {
    if (reaction.emoji.name === '✅') {

      await movieNightsSchema.findOneAndUpdate({
        _id: reaction.message.id
      }, {
        $pull: {
          reactedUserIds: user.id
        }
      }, {
        upsert: true
      })
    }
  }

})


// Voice Channel Join/Leave
// bot.on('voiceStateUpdate', (oldState, newState) => {
//   // const aBlock_voiceChannel = message.guild.channels.cache.get('712285104332079144');
//   // var betarole = user.guild.roles.cache.get("712768970805084190");

//   // if the member didn't changed channel we exit this function
//   if (oldState.channelID == newState.channelID) return;

//   if (newState.channelID == '712390744328896542') {
//       // if he joined the channel
//       console.log('join');
//       // user.roles.add(betarole);
//   } else if (oldState.channelID == '712390744328896542') {
//       // if he leaved the channel
//       console.log('leave');
//       // user.roles.remove(betarole);
//   }
// });

// Spam filter
// const usersMap = new Map();

// bot.on('message', message => {
// if (message.author.bot) return;

// if(usersMap.has(message.author.id)) {
// const userData = usersMap.get(message.author.id);
// const msgCount = userData.msgCount;

// if (parseInt(msgCount) === 5) {
//   const parolerole = message.guild.roles.get("713007999009947648");

//   message.member.role.add(parolerole);
//   message.reply('You have been put on parole');
// }
// else {
//   msgCount++;
//   userData.msgCount = msgCount;
//   usersMap.set(message.author.id, userData);

// }
// }
// else {
// usersMap.set(message.author.id, {
//   msgCount: 1,
//   lastMessage: message,
//   timer: null
// });
// setTimeout(() => {
//   usersMap.delete(message.author.id);
// }, 5000);
// }
// });

// // #chess
// bot.on('message', message => {
//   const CHANNEL = message.guild.channels.cache.get('727132751299346533');
//   if (message.channel == CHANNEL) {
//     message.react('🎗️');
//   }
// });

// Reaction roles - Beta (Currently used: Chess Tournament)
// bot.on('messageReactionAdd', async (reaction, user) =>{
//   const reactionTesting = bot.channels.cache.get("742134589471981658");

//   var reactUserID = user.id;
//   var reactionss = reaction.message.guild.members.cache.get(reactUserID);
//   if(reaction.message.partial) await reaction.message.fetch();
//   if (reaction.partial) await reaction.fetch();
//   if(user.bot) return;
//   // if(!reaction.message.guild) return;
//   // if(reaction.message.guild.id !== '711634547770654791') return;
//   // if(reaction.message.channel.id === '742134589471981658') {
//   if (reaction.message.id === '742136932666048543'){
//     // if(reaction.emoji.name === '🎗️') {
//     //   await reaction.message.guild.members.cache.get(user.id).roles.add("731069380959535145")
//     //   // .then (chessmod.send("<@!" + user.id + "> just opted for chess."))
//     //   return;
//     // }
//     if(reaction.emoji.name === '🔨') {
//        reactionTesting.send('reacted on Hammer').then(m=>m.delete({timeout:5000}));
//       // await reaction.message.guild.members.cache.get(user.id).roles.add("720413313011417118")
//       // return;

//       const userReactions = reaction.message.cache.get(reacted => reacted.users.cache.has(reactUserID));
//             try {
//               for (const reaction of userReactions.values()) {
//                 await reaction.users.remove(userId);
//               }
//             } catch (error) {
//               console.error('Failed to remove reactions.');
//             }

//     }
//     if(reaction.emoji.name === '🔔') {
//        reactionTesting.send('reacted on bell').then(m2=>m2.delete({timeout:5000}));
//       // await reaction.message.guild.members.cache.get(user.id).roles.add("720413313011417118")
//       // return;

//     }
// }
// else {
//   return;
// }

// });

// bot.on('messageReactionRemove', async (reaction, user) =>{
//   // var chessmod = bot.guild.channels.cache.get("731076365318881280");
//   if(reaction.message.partial) await reaction.message.fetch();
//   if (reaction.partial) await reaction.fetch();
//   if(user.bot) return;
//   if(!reaction.message.guild) return;
//   if(reaction.message.guild.id !== '711634547770654791') return;
//   if(reaction.message.channel.id === '727132751299346533') {
//     if(reaction.emoji.name === '🎗️') {
//       await reaction.message.guild.members.cache.get(user.id).roles.remove("731069380959535145")
//       // chessmod.send("<@!" + user.id + "> opted out of chess.")
//       return;
//     }
//     if(reaction.emoji.name === '🔨') {
//       await reaction.message.guild.members.cache.get(user.id).roles.remove("720413313011417118")
//       return;
//   }
// }
// else {
//   return;
// }});

// // Reaction channels - Beta
// bot.on('messageReactionAdd', async (reaction, user) =>{
//   if(reaction.message.partial) await reaction.message.fetch();
//   if (reaction.partial) await reaction.fetch();
//   if(user.bot) return;
//   if(!reaction.message.guild) return;
//   if(reaction.message.guild.id !== '711634547770654791') return;
//   if(reaction.message.id === '720996172570951832') {
//     if(reaction.emoji.name === '⚽') {
//       let guild = bot.guild.channels.cache.get("")
//     }
//     if(reaction.emoji.name === '🔨') {
//       await reaction.message.guild.members.cache.get(user.id).roles.add("720413313011417118")
//       return;
//   }}
// else {
//   return;
// }});

// react to get inmate role - BETA (CHANGE THE CODE)
//  bot.on('raw', payload =>{
//     console.log(payload);
// //   if(payload.t === 'MESSAGE_REACTION_ADD') {
// //     console.log('reacted');
// //   if(payload.d.emoji.name != '⚽') return;
// //     if(payload.d.message_id === '720996172570951832') {
// //       console.log('correct message');
// //       let channel = bot.channels.cache.get(payload.d.channel_id)
// //        if(channel.messages.cache.has(payload.d.message_id)) {
// //          console.log('cached message')
// //          .then (message => {
// //            let reaction = messages.reaction.get('⚽')
// //            let user = bot.users.cache.get(payload.d.uaer_id)
// //            bot.emit('messageReactionAdd'. reaction, user);
// //          })
// //        }
// //        else {
// //          channel.messages.cache.fetch(payload.d,message_id)
// //          .then (console.log("mssage fetched"))
// //          .catch(err => console.log(err));
// //        }
// //     }
// //  }

//  });
// bot.on('messageReactionAdd', (reaction, user) => {
//   console.log('reaction identified');
//   console.log(user.username+" reacted with "+reaction.emoji.name);
// });

// bot.on('messageReactionRemove', async (client,message,member) =>{
// // const welc = message.guild.channels.cache.get('720996114786025472');
// // const welcc = welc.id;
// var role2 = member.guild.role.cache.get("712768970805084190");
// //var member = member.id;

// if (message.react == '⚽') {
//   member.role.add(role2);
// }
// });

// // flash-news - beta
// bot.on('message', async(message) => {
//   const updatechannel = message.guild.channels.cache.get('722211270962577488');
//   const logchannel = message.guild.channels.cache.get('723646089399369869')
//   var delmessage = message.content;
//   var messageauthor = ("<@"+message.author.id+'>');
//   var lastmessageid = message.id;
//   if (message.channel == updatechannel) {
//   if (message.author.bot) return;
//     // message.delete({timeout: 1000}).catch(err => console.log(err))
//     // .then (message.channel.send(delmessage))
//      (logchannel.send(delmessage));
//   }
// });

//   // annonymous chat - beta
// bot.on('message', async(message) => {
//   const annonchan1 = message.guild.channels.cache.get('734209898270949406');
//   const annonchan2 = message.guild.channels.cache.get('734215442511560804');
//   // var messageauthor = ("<@"+message.author.id+'>');
//   // var lastmessageid = message.id;
//   // if (message.author.bot) return;
//   if(message.channel == annonchan1) {
//     if (message.content.startsWith('robo')) {
//       if (!message.member.hasPermission('KICK_MEMBERS')) return;
//       annonchan2.send("--say "+message.content);
//       // message.react("👍");
//     };
//     // annonchan2.send("<@!"+message.author.id+">, '" + message.content+"'.");
//     // message.delete({timeout: 5000}).catch(err => console.log(err));
//   };
//   if(message.channel == annonchan2) {
//     if(message.author.id == '712970953298149426') return;
//     annonchan1.send(message.content);
//   };
// });

// // react nou - temp
// bot.on('message', message => {
//   if (message.content.includes('you')) {
//     message.react(':nou:')
//   if (message.content.includes.toLowerCase() === 'u' .toLocaleLowerCase()) {
//     message.react(':nou:')
//     }
//     });

// // RIP
// bot.on('message', message => {
//   if (message.content.toLowerCase() === 'rip'.toLocaleLowerCase()) {
//   message.react('🇷') & message.react('🇮') & message.react('🇵')
//   }
//   });

// // polls - Beta
// bot.on('message', message => {

//   let args = message.content.substring(PREFIX.length).split(" ");

//   switch (args[1]) {

//       case 'poll':

//           const pollEmbed = new Discord.MessageEmbed()
//               .setColor(0xFFC300)
//               .setTitle("**Creating a Poll!**")
//               .setDescription("poll is to create a yes or no poll");

//           if (!args[2]) {
//               message.channel.send(pollEmbed);
//               break;

//           }

//           let msgArgs = args.slice(1).join(" ");

//           const pollMessage = new Discord.MessageEmbed()
//               .setColor(0xFFC300)
//               .setTitle("New Poll Received!")
//               .setDescription("Submitted by " + message.author.username)
//               .setThumbnail(message.author.avatarURL)
//               .addField("**Message:**", msgArgs)
//               .setFooter(message.guild.name);

//               message.channel.send(pollMessage).then(messageReaction => {
//               messageReaction.react("👍");
//               messageReaction.react("👎");
//               message.delete(100).catch(console.error);
//               });
//               break;

//   }

// });

// // role assingment
// bot.on('message', message => {
//   const CHANNEL = message.guild.channels.cache.get('727551487915327488');
//     let args = message.content.substring(PREFIX.length).split(" ");
//     switch(args[0]){
//         case 'parole' :
//           var person = message.guild.cache.member(message.mentions.users.first() || message.guild.members.cache.get(args[1]))
//           if(!person) return message.channel.send("Couldn't find that user");

//           var inmaterole = member.guild.roles.cache.get("712289127948877844");
//           var parolerole = member.guild.roles.cache.get("713007999009947648");

//           person.roles.remove(inmaterole);
//           person.roles.add(parolerole);
//           message.react('👍');

//           break;
//         }
// });

//           <......... The functional features ends here .........>

// //  Messages
// bot.on('message', message=>{
//     let args = message.content.substring(PREFIX.length).split(" ");
//     switch(args[1]){
//         case 'ping':
//         message.channel.send('pong!');
//             break;
//         case 'website':
//         message.channel.send('https://www.amnuz.com');
//             break;
//         case 'author':
//         message.channel.send('**Mohammed Farish** from Amnuz Technologies https://www.amnuz.com');
//             break;
//         case 'test':
//         message.channel.send('Test Success!');
//             break;
//         case 'lookupdate':
//         message.delete({timeout: 1000}).catch(err => console.log(err));
//         message.channel.send("Yes, and this is how I look now. I've been crawling around the internet, in search of colours, and nothing felt as perfect as purple. I'd like to thank the Prison Officers for giving me this new look. I love it!");
//         message.channel.send("Also, I can't wait to pelt some of the inmates after the escape that you've been planning in here. :wink:");
//         break;

//     }
// });

bot.login(process.env.DJS_BOT);
