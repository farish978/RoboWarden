const mongoose = require('mongoose');

const movieNightsSchema = mongoose.Schema({
    _id: String,
    reactedUserIds: [String],
    streamer: String,
    streamerId: String
});


module.exports = mongoose.model('movienight', movieNightsSchema);