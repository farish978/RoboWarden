const mongoose = require('mongoose');

const messageCountSchema = mongoose.Schema({

    //message sender id
    _id: {
        type: String
    },
    // message count
    messageCount: {
        type: Number
    },
    Name: {
        type: String
    },
    username: {
        type: String
        },
    usernameHistory: [String],
    roles: [String],
    paroleStatus: String,
    paroleChannelId: String
})

module.exports = mongoose.model('message-count', messageCountSchema);