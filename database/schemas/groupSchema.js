const mongoose = require('mongoose');

const reqString = {
    type: String,
    required: true
};

const groupSchema = mongoose.Schema({
    groupName: String,
    groupDescription: String,
    groupCreator: String,
    groupCreatorId: String,
    groupCreatedAt: String,
    groupMembers: [String],
    groupStatus: String
});

module.exports = mongoose.model('group', groupSchema);