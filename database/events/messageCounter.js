const mongo = require('../dbConnect');
const messageCountSchema = require('../schemas/messageCountSchema');

module.exports = (bot) => {

    bot.on('message', async (message) => {
        if (message.guild.id !== '711634547770654791') return;
        if (message.author.bot) return;
        if (message.channel.id === '727675902456234036' || message.channel.parent.id === '737682411818582098') return;


        const { author } = message;
        const { id } = author
        const { username } = author
        const displayName = message.member.displayName;


        // console.log(username);
        // console.log(displayName);

        // console.log(author);
        // console.log(username);

        await messageCountSchema.findOneAndUpdate({
            _id: id
        },
            {
                $inc: {
                    'messageCount': 1,
                },

                Name: displayName,
                username: username,
                $addToSet: {
                    usernameHistory: username,
                },
                roles: message.member.roles.cache.keyArray()
            },
            {
                upsert: true
            }).exec()
    })
}