module.exports = bot => {

  bot.on('message', async (message) => {
    const movieNightsSchema = require('../database/schemas/movieNightsSchema');


    // Reactions for F
    if (message.content.toLowerCase() === 'F'.toLocaleLowerCase()) {
      message.react('🇫');
    };
    if (message.content.startsWith('F ')) (
      message.react('🇫')
    );
    if (message.content.startsWith('f ')) (
      message.react('🇫')
    );




    //feature request channel
    const featurerequestChannel = message.guild.channels.cache.get('712640459473813574');
    if (message.channel == featurerequestChannel) {
      message.react('👍').then(() => message.react('👎'));
    };


    //polls channel
    const pollsChannel = message.guild.channels.cache.get('712412653338886185');
    if (message.channel == pollsChannel) {
      message.react('🇦').then(() => message.react('🅱️'));
    };


    //movie night channel
    const movienightChannel = message.guild.channels.cache.get('715255733666709554');
    // const CHANNEL = message.guild.channels.cache.find(chan => 
    //   chan.name === 'movie-night');
    if (message.channel == movienightChannel) {
      message.react('✅');

      await movieNightsSchema.findOneAndUpdate({
        _id: message.id
      }, {
        streamer: message.member.displayName,
        streamerId: message.author.id
      }, {
        upsert: true
      });
    };


  });


}
