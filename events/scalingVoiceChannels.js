const channelName = `🎤 Inmates C-Block`;
const channelName2 = `🎤 Inmates A1 Block`;

const getVoiceChannels = (guild) => {
  return guild.channels.cache.filter((channel) => {
    return channel.type === 'voice' && channel.name === channelName
  })
}

module.exports = (bot) => {
  bot.on('voiceStateUpdate', (oldState, newState) => {
    if (getVoiceChannels !== getVoiceChannels) return;

    const { guild } = oldState
    const joined = !!newState.channelID

    const channelId = joined ? newState.channelID : oldState.channelID
    let channel = guild.channels.cache.get(channelId)

    // console.log(`${newState.channelID} vs ${oldState.channelID} (${channel.name})`)

    if (channel.name === channelName) {
      if (joined) {
        const channels = getVoiceChannels(guild)

        let hasEmpty = false

        channels.forEach((channel) => {
          if (!hasEmpty && channel.members.size === 0) {
            hasEmpty = true
          }
        })

        if (!hasEmpty) {
          const {
            type,
            userLimit,
            bitrate,
            parentID,
            permissionOverwrites,
            rawPosition,
          } = channel

          guild.channels.create(channelName, {
            type,
            bitrate,
            userLimit,
            parent: parentID,
            permissionOverwrites,
            position: rawPosition,
          })
        }
      } else if (
        channel.members.size === 0 &&
        getVoiceChannels(guild).size > 1
      ) {
        channel.delete()
      }
    } else if (oldState.channelID) {
      channel = guild.channels.cache.get(oldState.channelID)
      if (
        channel.name === channelName &&
        channel.members.size === 0 &&
        getVoiceChannels(guild).size > 1
      ) {
        channel.delete()
      }
    }

    // ==========     Prod call   =================

    const prodChat = bot.channels.cache.get('753122371279257621');

    if (newState.member.id === '252128902418268161') return;
    if (oldState.member.id === '252128902418268161') return;


    if (newState.channelID === '753114986011951184') {

      prodChat.updateOverwrite(newState.member.id, {
        SEND_MESSAGES: true,
        ADD_REACTIONS: true
      })
      return;

    } else if (oldState.channelID === '753114986011951184') {

      prodChat.updateOverwrite(oldState.member.id, {
        SEND_MESSAGES: false,
        ADD_REACTIONS: false
      })
      return;

    };
    // ================================

  })
}