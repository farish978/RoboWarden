module.exports = (bot) => {
  bot.on("message", (message) => {
    const messageContent = message.content.toLowerCase();
    const nwordwarning =
      "this is a verbal warning for the use of the n-word, which is against the server rules. Kindly refrain yourself from using such words or having any sort of racism behaviour. You can reread the <#712285292366921781> to refresh your mind or you can tag `@Warden 👮` and they shall help you further.";

    if (messageContent.includes("nibba") ||
      messageContent.includes("nibbi") ||
      messageContent.includes('nigga') ||
      messageContent.includes('nigger') ||
      messageContent.includes('negro') ||
      messageContent.includes('nigro')) {
      message.reply(nwordwarning);
    };
  });
};
